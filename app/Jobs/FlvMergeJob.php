<?php

namespace App\Jobs;

use App\Services\GetFlvService;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class FlvMergeJob extends Job
{


    protected $files;
    protected $merge_save_path;
    public function __construct($files,$merge_save_path)
    {
	    $this->files = $files;
	    $this->merge_save_path = $merge_save_path;
    }


    public function handle()
    {
	if( count($this->files) == 1){
	    rename($this->files[0],$this->merge_save_path);
    }

	//文件名特殊 可能会导致ffmpeg失败
    $files_str = '';
    $tmp_name = '';
    foreach ($this->files as $file){
        $tmp_name .= md5_file($file);
        $files_str .= "file '$file'".PHP_EOL;
    }
    $txt_tmp_name = md5($tmp_name).'.txt';
    file_put_contents($txt_tmp_name,$files_str);

	system("ffmpeg -f concat -safe 0 -i $txt_tmp_name -c copy $tmp_name.flv");

	rename($tmp_name.'.flv',$this->merge_save_path);
	$this->rmFile($txt_tmp_name);
	foreach($this->files as $file)
	$this->rmFile($file);
    }

    protected function rmFile($filepath)
    {
	    if( file_exists($filepath))
	    unlink($filepath);
    }



}
