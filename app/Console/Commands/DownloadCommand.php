<?php

namespace App\Console\Commands;

use App\Jobs\BiliBiliVideoDownloadJob;
use App\Services\GetPagesService;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class DownloadCommand extends Command
{
    protected $signature = 'down {bvid?}';

    protected $description = '下载哔哩哔哩视频 ';

    public function handle()
    {
        $bvid = $this->argument('bvid');

        $getPageService = new GetPagesService($bvid);
        $pages = $getPageService->get();
        foreach ($pages as $page){
         $page->bvid = $bvid;
         dispatch(new BiliBiliVideoDownloadJob($page));
        }

    }
}

