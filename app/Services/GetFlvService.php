<?php

namespace App\Services;


use http\Exception\RuntimeException;
use Illuminate\Support\Facades\Http;

class GetFlvService{

    protected $bvid,$cid;
    const API = 'https://api.bilibili.com/x/player/playurl?bvid=%s&cid=%s&qn=%s&otype=json';
    public function __construct(string $bvid,int  $cid)
    {
        $this->bvid = $bvid;
        $this->cid = $cid;
    }
    public function get()
    {
        $url = sprintf(self::API,$this->bvid,$this->cid,80);
        $res = Http::withHeaders(
         ['Cookie' =>  env('BILI_COOKIE')]
        )->get($url);

        $res = json_decode($res->body());

        if( $res->code != 0)
        throw new RuntimeException("获取失败");
        return $res->data->durl;
    }

}
