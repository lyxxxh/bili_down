<?php

namespace App\Services;


use http\Exception\RuntimeException;
use Illuminate\Support\Facades\Http;

class GetPagesService{

    protected $bvid;
    const API = 'https://api.bilibili.com/x/web-interface/view?bvid=';
    public function __construct(string $bvid)
    {
        $this->bvid = $bvid;
    }
    public function get()
    {
        $res = Http::get(self::API.$this->bvid);
        $res = json_decode($res->body());
        if( $res->code != 0)
        throw new RuntimeException("获取失败");
        return $res->data->pages;
    }

}
